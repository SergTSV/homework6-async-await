//  В асинхронних програмах, ви можете мати два рядки коду (Рядок2 після Рядка1), де Рядок1 містить команди, які будуть
//  виконуватися в майбутньому, а Рядок2 працює до завершення команди в Рядок1.



const Button = document.querySelector("button");
const ul = document.createElement("ul");

Button.addEventListener("click", getUserIPInfo);

async function getIpInfo(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

async function getUserIPInfo() {
    const dataIP = await getIpInfo('https://api.ipify.org/?format=json');

    const dataInfo = await getIpInfo(`http://ip-api.com/json/${dataIP.ip}?fields=country,regionName,city,district,continent`);

    for (let self in dataInfo) {
        createList(self, dataInfo[self]);
    }

    document.body.append(ul);
}

    function createList(self, info) {
    const li = document.createElement("li");
    li.innerHTML = `${self}: ${info}`;
    ul.append(li);
}
